﻿using System;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApppMvc.Models;
using System.Collections.Generic;

namespace WebApppMvc.Utilities
{
    public class ApiService
    {
        public static async Task<ResponseApi> Post<T>(string urlBase, string controller, T model)
        {
            try
            {
                var request = JsonConvert.SerializeObject(model);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var response = await client.PostAsync(controller, content);

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseApi
                    {
                        IsSuccess = false,
                        Message = response.StatusCode.ToString(),
                    };
                }

                var result = await response.Content.ReadAsStringAsync();

                return new ResponseApi
                {
                    IsSuccess = true,
                    Message = "Record added OK",
                    Result = result,
                };
            }
            catch (Exception ex)
            {
                return new ResponseApi
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }

        public static async Task<ResponseApi> GetId<T>(string urlBase, string controllerint, int id)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}/{1}", controllerint, id);
                var response = await client.GetAsync(url);

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseApi
                    {
                        IsSuccess = false,
                        Message = response.StatusCode.ToString(),
                    };
                }

                var result = await response.Content.ReadAsStringAsync();
                var list = JsonConvert.DeserializeObject<T>(result);
                return new ResponseApi
                {
                    IsSuccess = true,
                    Message = "Ok",
                    Result = list,
                };
            }
            catch (Exception ex)
            {
                return new ResponseApi
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }

        public static async Task<ResponseApi> Get<T>(string urlBase, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = controller;
                var response = await client.GetAsync(url);

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseApi
                    {
                        IsSuccess = false,
                        Message = response.StatusCode.ToString(),
                    };
                }

                var result = await response.Content.ReadAsStringAsync();
                var list = JsonConvert.DeserializeObject<List<T>>(result);
                return new ResponseApi
                {
                    IsSuccess = true,
                    Message = "Ok",
                    Result = list,
                };
            }
            catch (Exception ex)
            {
                return new ResponseApi
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }


        public static async Task<ResponseApi> Put<T>(string urlBase, string controller, T model)
        {
            try
            {
                var request = JsonConvert.SerializeObject(model);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var response = await client.PutAsync(controller, content);

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseApi
                    {
                        IsSuccess = false,
                        Message = response.StatusCode.ToString(),
                    };
                }              

                return new ResponseApi
                {
                    IsSuccess = true,
                    Message = "Record updated OK",
                };
            }
            catch (Exception ex)
            {
                return new ResponseApi
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }
    }
}