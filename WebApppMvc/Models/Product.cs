﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApppMvc.Models
{
    public class Product
    {
        [Key]
        public int IdPorduct { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(100, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string Name { get; set; }

        [Display(Name = "Valor Unidad")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal UnitValue { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }
    }
}