﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApppMvc.Models
{
    public class Sale
    {
        [Key]
        public int IdSale { get; set; }

        [Display(Name = "Producto")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [Range(1, int.MaxValue, ErrorMessage = "Seleccione un valor")]
        public int IdProduct { get; set; }

        [Display(Name = "Cliente")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [Range(1, int.MaxValue, ErrorMessage = "Seleccione un valor")]
        public int IdClient { get; set; }

        [Display(Name = "Cantidad")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Range(1, int.MaxValue, ErrorMessage = "El Valor debe ser mayor a 0")]
        public decimal Amount { get; set; }

        [Display(Name = "Valor Unitario")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Range(1, int.MaxValue, ErrorMessage = "El Valor debe ser mayor a 0")]
        public decimal UnitValue { get; set; }

        [Display(Name = "Valor Total")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [Range(1, int.MaxValue, ErrorMessage = "El Valor debe ser mayor a 0")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal TotalValue { get; set; }

        [Display(Name = "Cliente")]
        public string NameProduct { get; set; }

        [Display(Name = "Producto")]
        public string NameClient { get; set; }

    }
}