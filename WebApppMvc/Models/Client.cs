﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApppMvc.Models
{
    public class Client
    {
        [Key]
        public int IdClient { get; set; }

        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string Cedula { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(100, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(100, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string LastName { get; set; }

        [Display(Name = "Telefono")]
        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(10, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string Phone { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }
    }
}