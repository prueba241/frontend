﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebApppMvc.Models;
using WebApppMvc.Utilities;

namespace WebApppMvc.Controllers
{
    public class ProductController : Controller
    {
        private static string urlBase = "";
        public ProductController()
        {
            urlBase = WebConfigurationManager.AppSettings["UrlApi"];
        }

        // GET: Product
        public async Task<ActionResult> Index()
        {
            var response = await ApiService.Get<Product>(urlBase, "product");

            if (!response.IsSuccess)
            {
                return View();
            }
           return View(response.Result); ;
        }

        // GET: Product/Details/5
        public async Task<ActionResult> Details(int id)
        {
            return await GetId(id);
        }


        private async Task<ActionResult>  GetId(int id)
        {
            try
            {
                var response = await ApiService.GetId<Product>(urlBase, "product", id);

                if (response.IsSuccess)
                {
                    return View(response.Result);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public async Task<ActionResult> Create(Product product)
        {
            try
            {
                product.Active = true;
                var response = await ApiService.Post<Product>(urlBase, "product", product);

                if (!response.IsSuccess)
                {
                    return View(product);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(product);
            }
        }

        // GET: Product/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            return await GetId(id);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(Product product)
        {
            try
            {
                var response = await ApiService.Put<Product>(urlBase, "product", product);

                if (!response.IsSuccess)
                {
                    return View(product);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(product);
            }
        }


      

    }
}
