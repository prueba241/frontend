﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebApppMvc.Models;
using WebApppMvc.Utilities;

namespace WebApppMvc.Controllers
{
    public class ClientController : Controller
    {
        private static string urlBase = "";
        public ClientController()
        {
            urlBase = WebConfigurationManager.AppSettings["UrlApi"];
        }

        // GET: Client
        public async Task<ActionResult> Index()
        {
            var response = await ApiService.Get<Client>(urlBase, "client");

            if (!response.IsSuccess)
            {
                return View();
            }
            return View(response.Result); ;
        }

        // GET: Client/Details/5
        public async Task<ActionResult> Details(int id)
        {
            return await GetId(id);
        }

        private async Task<ActionResult> GetId(int id)
        {
            try
            {
                var response = await ApiService.GetId<Client>(urlBase, "client", id);

                if (response.IsSuccess)
                {
                    return View(response.Result);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }


        // GET: Client/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        public async Task<ActionResult> Create(Client client)
        {
            try
            {
                client.Active = true;
                var response = await ApiService.Post<Client>(urlBase, "client", client);

                if (!response.IsSuccess)
                {
                    return View(client);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(client);
            }
        
        }

        // GET: Client/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            return await GetId(id);
        }

        // POST: Client/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(Client client)
        {
            try
            {
                var response = await ApiService.Put<Client>(urlBase, "client", client);

                if (!response.IsSuccess)
                {
                    return View(client);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(client);
            }
        }


    }
}
