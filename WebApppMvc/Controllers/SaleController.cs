﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebApppMvc.Models;
using WebApppMvc.Utilities;

namespace WebApppMvc.Controllers
{
    public class SaleController : Controller
    {
        private static string urlBase = "";
        public SaleController()
        {
            urlBase = WebConfigurationManager.AppSettings["UrlApi"];
        }

        // GET: Product
        public async Task<ActionResult> Index()
        {
            var response = await ApiService.Get<Sale>(urlBase, "sale");

            if (!response.IsSuccess)
            {
                return View();
            }
            return View(response.Result); ;
        }


        public async Task<ActionResult> Create(int id)
        {
            var response = await ApiService.GetId<Product>(urlBase, "product", id);

            if (!response.IsSuccess)
            {
                return RedirectToAction("Index");
            }
            var product = (Product)response.Result;
            await CreateListClient();
            var data = new Sale();
            data.IdProduct = id;
            data.UnitValue = product.UnitValue;
            data.NameProduct = product.Name;
            return View(data);
        }

        [HttpPost]
        public async Task<ActionResult> Create(Sale sale)
        {
            var response = await ApiService.Post<Sale>(urlBase, "sale", sale);

            if (!response.IsSuccess)
            {
                await CreateListClient(sale.IdClient);
                return View(sale);
            }

            return RedirectToAction("Index");
        }

        private async Task CreateListProducts(int idSelect = 0)
        {
            var list = await ApiService.Get<Product>(urlBase, "product");
            List<Product> items = (List<Product>)list.Result;
            Session["ListProducts"] = items;
            var listActive  = items.Where(a => a.Active == true).OrderBy(a => a.Name).ToList();
            listActive.Add(new Product { IdPorduct = 0, Name = "[Seleccione un Producto...]" });
            ViewBag.IdProduct = new SelectList(listActive, "IdPorduct", "Name", idSelect);
        }

        private async Task CreateListClient(int idSelect = 0)
        {
            var list = await ApiService.Get<Client>(urlBase, "client");
            List<Client> items = (List<Client>)list.Result;
            var listActive = items.Where(a => a.Active == true).OrderBy(a => a.Name).ToList();
            listActive.Add(new Client { IdClient = 0, Name = "[Seleccione un Cliente...]" });
            ViewBag.IdClient = new SelectList(listActive, "IdClient", "Name", idSelect);
        }


        public async Task<ActionResult> SaleClient(int id)
        {
            var response = await ApiService.GetId<Client>(urlBase, "client", id);

            if (!response.IsSuccess)
            {
                Session.Remove("addProduct");
                return RedirectToAction("Index");
            }

            var client = (Client)response.Result;
            await CreateListProducts();
            var data = new Sale();
            data.IdClient = id;
            data.NameClient = string.Format("{0} {1}", client.Name,client.LastName) ;
            return View(data);
        }

        [HttpPost]
        public async Task<ActionResult> SaleClient(Sale sale)
        {
            
            if (Session["addProduct"] == null)
            {
                List<Sale> sales = new List<Sale>();
                sales.Add(sale);
                Session["addProduct"] = sales;
            }
            else
            {
                List<Sale> sales = (List<Sale>)Session["addProduct"];
                sales.Add(sale);
                Session["addProduct"] = sales;

            }

            await CreateListProducts();
            var data = new Sale();
            ModelState.Clear();
            data.IdClient = sale.IdClient;
            data.NameClient = sale.NameClient;
            return View(data);

        }


        public ActionResult GetProduct(int id)
        {
            var listProdus = (List<Product>)Session["ListProducts"];
            var product = listProdus.Where(a => a.IdPorduct == id).First();
            //return product;
            return Json(product, JsonRequestBehavior.AllowGet);
        }


      
        public async Task<ActionResult> SaveProduts()
        {
            List<Sale> sales = (List<Sale>)Session["addProduct"];

            var response = await ApiService.Post<List<Sale>>(urlBase, "sale/saleClient", sales);

            Session.Remove("addProduct");
            return RedirectToAction("Index");
        }
    }
}
